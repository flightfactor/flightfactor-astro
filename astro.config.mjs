import { defineConfig } from 'astro/config';

const DEV_PORT = 3000;

// import lit from "@astrojs/lit";
console.log(process.env.NODE_ENV)

const WORKIN_ENV = process.env.NODE_ENV

let sitevar = 'https://kaksico.com/'
if (WORKIN_ENV == 'development') {

  sitevar = `http://localhost:${DEV_PORT}`

}

// import lit from "@astrojs/lit";

// https://astro.build/config
export default defineConfig({

  site: sitevar,
  base: '/',

  experimental: {
    assets: true
  },


});